<?php
$base = '../../includes/';
include $base . "header.php";

?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";

            // Attempt select query execution
            $sql = "SELECT * FROM student";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {

                echo "<table class='table'>";
                echo "<tsection>";
                echo "<tr>";
                echo "<th>Student ID</th>";
                echo "<th>Student Name</th>";
                echo "<th>Student Roll Number</th>";
                echo "<th>Student Class</th>";
                echo "</tr>";
                echo "</tsection>";
                echo "<tbody>";
                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['student_name'] . "</td>";
                    echo "<td>" . $row['student_rollnumber'] . "</td>";
                    echo "<td>" . $row['student_class'] . "</td>";
                    echo "<td>";
                    echo "<a href='../result/index.php?id=" . $row['id'] . " 'title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                echo "</tbody>";
                echo "</table>";

                mysqli_free_result($result);
            } else {
                echo "0 results";
            }
            $conn->close();
            ?>
        </div>
    </div>
</div>
<?php
include $base . "footer.php";
?>
