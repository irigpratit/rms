<?php

$base = '../../includes/';

include_once $base . "common/dbconfig.php";

$student_name = $student_class = $student_rollnumber ="";

$name_err = $class_err = $rollnumber_err =  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //validation of student Name
    $input_student_name = trim($_POST["student_name"]);
    if (empty($input_student_name)) {
        $name_err = "Please enter a name for student.";
    } else {
        $student_name = $input_student_name;
    }

    //validation of student class
    $input_student_class = trim($_POST["student_class"]);
    if (empty($input_student_class)) {
        $class_err = "Please enter student in rollnumber.";
    } else {
        $student_class = $input_student_class;
    }

    //validation of student rollnumber number
    $input_student_rollnumber = trim($_POST["student_rollnumber"]);
    if (empty($input_student_rollnumber)) {
        $rollnumber_err = "Please enter student in rollnumber.";
    } else {
        $student_rollnumber = $input_student_rollnumber;
    }



    if (empty($name_err) && empty($rollnumber_err) && empty($class_err)) {
        if ($_SERVER["REQUEST_METHOD"] == ["POST"]) {
            $student_name = filter_input(INPUT_POST, 'student_name');
            $student_class = filter_input(INPUT_POST, 'student_class');
            $student_rollnumber = filter_input(INPUT_POST, 'student_rollnumber');
        }


        $sql = "INSERT INTO student (student_name,student_class, student_rollnumber)
        VALUES ('$student_name','$student_class','$student_rollnumber')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}

$conn->close();

?>


    <!--form input-->
<?php include $base . 'header.php'; ?>
    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                        <label>Student Name</label>
                        <input type="text" name="student_name" class="form-control"
                               value="<?php echo $student_name; ?>">
                        <span class="help-block"><?php echo $name_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($rollnumber_err)) ? 'has-error' : ''; ?>">
                        <label>Student rollnumber Number</label>
                        <input type="number" name="student_rollnumber" class="form-control"
                               value="<?php echo $student_rollnumber; ?>">
                        <span class="help-block"><?php echo $rollnumber_err; ?></span>
                    </div>


                    <?php include $base . "common/dbconfig.php"; ?>

                    <div class="form-group <?php echo (!empty($class_err)) ? 'has-error' : ''; ?>">
                        <label>Student Class</label>
                        <select name="student_class" class="form-control">
                            <?php
                            $sql = "SELECT id,class_name FROM class";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['id'] .">" . $row['class_name'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>
                        <span class="help-block"><?php echo $class_err; ?></span>
                    </div>

                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="<?php echo $URL; ?>index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
<?php include $base . 'footer.php'; ?>