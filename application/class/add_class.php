<?php

$base = '../../includes/';

include_once $base . "common/dbconfig.php";

$class_name = "";

$name_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //validation of Class Name
    $input_class_name = trim($_POST["class_name"]);
    if (empty($input_class_name)) {
        $name_err = "Please enter a name for class.";
    } else {
        $class_name = $input_class_name;
    }


    if (empty($name_err)) {
        if ($_SERVER["REQUEST_METHOD"] == ["POST"]) {
            $class_name = filter_input(INPUT_POST, 'class_name');
        }


        $sql = "INSERT INTO class (class_name)
        VALUES ('$class_name')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}

$conn->close();

?>

<!--Input Form-->

<?php include_once $base . 'header.php'; ?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">

            <div class="page-header">
                <h2>Create Record</h2>
            </div>
            <p>Please fill this form and submit to add class record to the database.</p>

            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">

                <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                    <label>Class Name</label>
                    <input type="text" name="class_name" class="form-control" value="<?php echo $class_name; ?>">
                    <span class="help-block"><?php echo $name_err; ?></span>
                </div>

                <input type="submit" class="btn btn-primary" value="submit">
                <a href="<?php echo $URL; ?>index.php" class="btn btn-default">Cancel</a>

            </form>
        </div>
    </div>
</div>
<?php include $base . 'footer.php'; ?>


