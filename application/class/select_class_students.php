<?php
$base = '../../includes/';
include $base . "header.php";

if(isset($_GET['exam_id'])) $exam="&exam_id=".$_GET['exam_id'];
else $exam='';

?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";

            // Attempt select query execution
            $id = $_GET['id'];
            $sql = "SELECT student.id,student.student_name,student.student_rollnumber,class.class_name 
                    FROM student 
                    INNER JOIN class on  class.id=student.student_class 
                    WHERE student.student_class=$id";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {

                echo "<table class='table'>";
                echo "<tsection>";
                echo "<tr>";
                echo "<th>Student ID</th>";
                echo "<th>Student Name</th>";
                echo "<th>Student Roll Number</th>";
                echo "<th>Student Class</th>";
                echo "</tr>";
                echo "</tsection>";
                echo "<tbody>";
                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['student_name'] . "</td>";
                    echo "<td>" . $row['student_rollnumber'] . "</td>";
                    echo "<td>" . $row['class_name'] . "</td>";
                    echo "<td>" . $row['section_name'] . "</td>";
                    echo "<td>";
                    echo "<a href='../result/display_result.php?id=" . $row['id'] . $exam. "' title='View Result' data-toggle='tooltip'><span>View Result &nbsp; </span></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                echo "</tbody>";
                echo "</table>";

                mysqli_free_result($result);
            } else {
                echo "0 results";
            }
            $conn->close();
            ?>
        </div>
    </div>
</div>
<?php
include $base . "footer.php";
?>
