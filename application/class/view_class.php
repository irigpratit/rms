<?php
$base = '../../includes/';
include $base . "header.php";


if(isset($_GET['exam_id'])) $exam="&exam_id=".$_GET['exam_id'];
else $exam='';
?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";

            // Attempt select query execution
            $sql = "SELECT * FROM class";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                echo "<table class='table'>";
                echo "<tsection>";
                echo "<tr>";
                echo "<th>Class ID</th>";
                echo "<th>Class Name</th>";
                echo "</tr>";
                echo "</tsection>";
                echo "<tbody>";
                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['class_name'] . "</td>";
                    echo "<td>";
                    echo "<a href='../class/select_class_students.php?id=" . $row['id'] .$exam. " 'title='Select Student' data-toggle='tooltip'><span class='btn btn-default'> Select </span></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                echo "</tbody>";
                echo "</table>";

                mysqli_free_result($result);
            } else {
                echo "0 results";
            }
            $conn->close();
            ?>
        </div>
    </div>
</div>
<?php
include $base . "footer.php";
?>
