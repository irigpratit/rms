<?php

$base = '../../includes/';

include_once $base . "common/dbconfig.php";

$student_name = $exammap_id = $obtained_marks = $subject_id = "";

$name_err = $exammapid_err = $marks_err = $subject_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //validation of student class
    $input_exammap_id = trim($_POST["exam_id"]);
    if (empty($input_exammap_id)) {
        $exammapid_err = "Please enter exam id";
    } else {
        $exam_id = $input_exammap_id;
    }


    //validation of student section
    $input_subject_id = trim($_POST["subject_id"]);
    if (empty($input_subject_id)) {
        $subject_err = "Please enter subject id.";
    } else {
        $subject_id = $input_subject_id;
    }


    //validation of student marks number
    $input_obtained_marks = trim($_POST["obtained_marks"]);
    if (empty($input_obtained_marks)) {
        $marks_err = "Please enter obtained marks.";
    } else {
        $obtained_marks = $input_obtained_marks;
    }

    $input_student_id = trim($_POST["student_id"]);
    if (empty($input_student_id)) {
        $marks_err = "Please enter obtained marks.";
    } else {
        $student_id = $input_student_id;
    }

    $input_class_id = trim($_POST["class_id"]);
    if (empty($input_class_id)) {
        $marks_err = "Please enter obtained marks.";
    } else {
        $class_id = $input_class_id;
    }


    if (empty($exammapid_err) && empty($marks_err) && empty($subject_err)) {
        if ($_SERVER["REQUEST_METHOD"] == ["POST"]) {
            $exam_id = filter_input(INPUT_POST, 'exam_id');
            $class_id= filter_input(INPUT_POST,'class_id');
            $subject_id = filter_input(INPUT_POST, 'subject_id');
            $student_id = filter_input(INPUT_POST, 'student_id');
            $obtained_marks = filter_input(INPUT_POST, 'obtained_marks');
        }


        $sql = "INSERT INTO marks (exam_id, subject_id, obtained_marks, class_id, student_id)
                VALUES ('$exam_id','$subject_id','$obtained_marks','$class_id','$student_id')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}


?>


    <!--form input-->
<?php include $base . 'header.php'; ?>
    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group <?php echo (!empty($exammapid_err)) ? 'has-error' : ''; ?>">
                        <label>Exam</label>
                        <select name="exam_id" class="form-control">

                            <?php
                            $sql = "SELECT id,exam_type FROM exam";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['id'] .">" . $row['exam_type']
                                        . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>

                        </select>
                        <span class="help-block"><?php echo $exammapid_err; ?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($subject_err)) ? 'has-error' : ''; ?>">
                        <label>Subject ID</label>
                        <select name="subject_id" class="form-control">

                            <?php
                            $sql = "SELECT id,subject_name FROM subject";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['id'] .">" . $row['subject_name'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>

                        </select>
                        <span class="help-block"><?php echo $subject_err; ?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($subject_err)) ? 'has-error' : ''; ?>">
                        <label>Class</label>
                        <select name="class_id" class="form-control">
                            <?php
                            $sql = "SELECT id,class_name FROM class";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['id'] .">" . $row['class_name'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>
                        <span class="help-block"><?php echo $subject_err; ?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($subject_err)) ? 'has-error' : ''; ?>">
                        <label>Student</label>
                        <select name="student_id" class="form-control">
                            <?php
                            $sql = "SELECT id,student_name FROM student";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=". $row['id'] .">" . $row['student_name'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>
                        <span class="help-block"><?php echo $subject_err; ?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($marks_err)) ? 'has-error' : ''; ?>">
                        <label>Student marks Number</label>
                        <input type="number" name="obtained_marks" class="form-control"
                               value="<?php echo $obtained_marks; ?>">
                        <span class="help-block"><?php echo $marks_err; ?></span>
                    </div>

                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="<?php echo $URL; ?>index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
<?php include $base . 'footer.php'; ?>