<?php

$base = '../../includes/';

include_once $base . "common/dbconfig.php";

$subject_name = $subject_fullmark = $subject_passmark = "";

$name_err = $fullmark_err = $passmark_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //validation of subject Name
    $input_subject_name = trim($_POST["subject_name"]);
    if (empty($input_subject_name)) {
        $name_err = "Please enter a name for subject.";
    } else {
        $subject_name = $input_subject_name;
    }

    //validation of subject fullmark
    $input_subject_fullmark = trim($_POST["subject_fullmark"]);
    if (empty($input_subject_fullmark)) {
        $fullmark_err = "Please enter value in fullmark.";
    } else {
        $subject_fullmark = $input_subject_fullmark;
    }

    //validation of subject passmark
    $input_subject_passmark = trim($_POST["subject_passmark"]);
    if (empty($input_subject_passmark)) {
        $passmark_err = "Please enter passmark.";
    } else {
        $subject_passmark = $input_subject_passmark;
    }


    if (empty($name_err) && empty($fullmark_err) && empty($passmark_err)) {
        if ($_SERVER["REQUEST_METHOD"] == ["POST"]) {
            $subject_name = filter_input(INPUT_POST, 'subject_name');
            $subject_fullmark = filter_input(INPUT_POST, 'subject_fullmark');
            $subject_passmark = filter_input(INPUT_POST, 'subject_passmark');
        }


        $sql = "INSERT INTO subject (subject_name,subject_fullmark, subject_passmark)
        VALUES ('$subject_name','$subject_fullmark','$subject_passmark')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}

$conn->close();

?>

<!--Input Form-->

<?php include_once $base . 'header.php'; ?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">

            <div class="page-header">
                <h2>Create Record</h2>
            </div>
            <p>Please fill this form and submit to add subject record to the database.</p>

            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">

                <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                    <label>Subject Name</label>
                    <input type="text" name="subject_name" class="form-control" value="<?php echo $subject_name; ?>">
                    <span class="help-block"><?php echo $name_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($fullmark_err)) ? 'has-error' : ''; ?>">
                    <label>Subject Fullmark</label>
                    <input type="number" name="subject_fullmark" class="form-control"
                           value="<?php echo $subject_fullmark; ?>">
                    <span class="help-block"><?php echo $fullmark_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($passmark_err)) ? 'has-error' : ''; ?>">
                    <label>Pass Mark</label>
                    <input type="number" name="subject_passmark" class="form-control"
                           value="<?php echo $subject_passmark; ?>">
                    <span class="help-block"><?php echo $passmark_err; ?></span>
                </div>
                <input type="submit" class="btn btn-primary" value="submit">
                <a href="<?php echo $URL; ?>index.php" class="btn btn-default">Cancel</a>

            </form>
        </div>
    </div>
</div>
<?php include $base . 'footer.php'; ?>


