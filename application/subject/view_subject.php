<?php
$base = '../../includes/';
include $base . "header.php";

?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";

            // Attempt select query execution
            $sql = "SELECT * FROM subject";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
//                while($row = $result->fetch_assoc()) {
                echo "<table class='table'>";
                echo "<tsection>";
                echo "<tr>";
                echo "<th>Subject ID</th>";
                echo "<th>Subject Name</th>";
                echo "<th>Full Marks</th>";
                echo "<th>Pass Marks</th>";
                echo "</tr>";
                echo "</tsection>";
                echo "<tbody>";
                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['subject_name'] . "</td>";
                    echo "<td>" . $row['subject_fullmark'] . "</td>";
                    echo "<td>" . $row['subject_passmark'] . "</td>";
                    echo "<td>";
//                            echo "<a href='views/read.php?id=". $row['id'] ." 'title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
//                            echo "<a href='views/delete.php?id=". $row['id'] ." 'title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                echo "</tbody>";
                echo "</table>";

                mysqli_free_result($result);
//                }
            } else {
                echo "0 results";
            }
            $conn->close();
            ?>
        </div>
    </div>
</div>
<?php
include $base . "footer.php";
?>
