<?php

$base = '../../includes/';

include_once $base . "common/dbconfig.php";

$class_id = $subject_id = "";

$classid_err = $subject_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {


    //validation of class class
    $input_class_id = trim($_POST["class_id"]);
    if (empty($input_class_id)) {
        $classid_err = "Please enter class in rollnumber.";
    } else {
        $class_id = $input_class_id;
    }


    //validation of class section
    $input_subject_id = trim($_POST["subject_id"]);
    if (empty($input_subject_id)) {
        $subject_err = "Please enter section.";
    } else {
        $subject_id = $input_subject_id;
    }


    if (empty($classid_err) && empty($subject_err)) {
        if ($_SERVER["REQUEST_METHOD"] == ["POST"]) {
            $class_id = filter_input(INPUT_POST, 'class_id');
            $subject_id = filter_input(INPUT_POST, 'subject_id');
        }


        $sql = "INSERT INTO classsubjectmap (class_id, subject_id)
        VALUES ('$class_id','$subject_id')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}


?>


    <!--form input-->
<?php include $base . 'header.php'; ?>
    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group <?php echo (!empty($classid_err)) ? 'has-error' : ''; ?>">
                        <label>Class ID</label>
                        <select name="class_id" class="form-control">
                            <?php
                            $sql = "SELECT id FROM class";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option>" . $row['id'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>
                        <span class="help-block"><?php echo $classid_err; ?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($subject_err)) ? 'has-error' : ''; ?>">
                        <label>Subject ID</label>
                        <select name="subject_id" class="form-control">
                            <?php
                            $sql = "SELECT id FROM subject";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option>" . $row['id'] . "</option>";
                                }
                                mysqli_free_result($result);
                            } else {
                                echo "0 results";
                            }
                            $conn->close();
                            ?>
                        </select>
                        <span class="help-block"><?php echo $subject_err; ?></span>
                    </div>

                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="<?php echo $URL; ?>index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
<?php include $base . 'footer.php'; ?>