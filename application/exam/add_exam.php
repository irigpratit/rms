<?php

$base = '../../includes/';

include_once $base . "common/dbconfig.php";

$exam_type = "";

$type_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //validation of exam type
    $input_exam_type = trim($_POST["exam_type"]);
    if (empty($input_exam_type)) {
        $type_err = "Please enter a type for exam.";
    } else {
        $exam_type = $input_exam_type;
    }


    if (empty($type_err)) {
        if ($_SERVER["REQUEST_METHOD"] == ["POST"]) {
            $exam_type = filter_input(INPUT_POST, 'exam_type');
        }


        $sql = "INSERT INTO exam (exam_type)
        VALUES ('$exam_type')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}

$conn->close();

?>

<!--Input Form-->

<?php include_once $base . 'header.php'; ?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">

            <div class="page-header">
                <h2>Create Record</h2>
            </div>
            <p>Please fill this form and submit to add exam record to the database.</p>

            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">

                <div class="form-group <?php echo (!empty($type_err)) ? 'has-error' : ''; ?>">
                    <label>exam type</label>
                    <input type="text" name="exam_type" class="form-control" value="<?php echo $exam_type; ?>">
                    <span class="help-block"><?php echo $type_err; ?></span>
                </div>

                <input type="submit" class="btn btn-primary" value="submit">
                <a href="<?php echo $URL; ?>index.php" class="btn btn-default">Cancel</a>

            </form>
        </div>
    </div>
</div>
<?php include $base . 'footer.php'; ?>


