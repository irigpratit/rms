<?php
$base = '../../includes/';
include $base . "header.php";
$total_marks=0;

if(isset($_GET['exam_id'])) $exam_id=(int)$_GET['exam_id'];
else $exam_id=0;

if(isset($_GET['id'])) $student_id=(int)$_GET['id'];
else $student_id=0;
?>


    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">
                <?php include $base . "common/dbconfig.php";

                $sql = "SELECT
                    m.obtained_marks, c.class_name,sub.subject_name,sub.subject_fullmark, sub.subject_passmark,
                    e.exam_type, s.student_name, s.student_rollnumber
                    FROM marks m
                    JOIN exam e ON e.id=m.exam_id AND e.id=$exam_id
                    JOIN student s ON m.student_id=s.id AND s.id=$student_id
                    JOIN subject sub ON m.subject_id=sub.id
                    JOIN class c ON c.id=m.class_id";

                if($exam_id==7){
                    $sql = "SELECT
                    avg(m.obtained_marks) AS 'obtained_marks', c.class_name,sub.subject_name,sub.subject_fullmark, sub.subject_passmark,
                    e.exam_type, s.student_name, s.student_rollnumber
                    FROM marks m
                    JOIN student s ON m.student_id=s.id AND s.id=$student_id
                    JOIN exam e ON e.id=m.exam_id
                    JOIN subject sub ON m.subject_id=sub.id
                    JOIN class c ON c.id=m.class_id GROUP BY m.id";
                    $exam_type="Final Exam";
                }


                $result = $conn->query($sql);

                $data = [];

                while($row = mysqli_fetch_array($result)){
                    if(!isset($exam_type)) $exam_type=$row['exam_type'];
                    if(!isset($student_nam)) $student_nam = $row['student_name'];
                    if(!isset($class_nam)) $class_nam = $row['class_name'];

                    if(!isset($new[$row['subject_name']])) {
                        $new[$row['subject_name']] = array(
                            "subject_fullmark" => $row['subject_fullmark'],
                            "final_fullmark" => $row['subject_fullmark'],
                            "subject_passmark" => $row['subject_passmark'],
                            "obtained_marks" => $row['obtained_marks']);
                    }else{
                        $new[$row['subject_name']]['final_fullmark']+=$row['subject_fullmark'];
                        $new[$row['subject_name']]['obtained_marks']+=$row['obtained_marks'];
                    }
                }



                ?>
                <h1 align="center">REPORT CARD</h1>
                <?php
                if (count($new) > 0) {
                    echo "<h4 align='center'><b>". $exam_type. "</b></h4>";
                    echo '<br>';
                    echo "<h4><b>Name Of Student:</b>". $student_nam. "</h4>";
                    echo "<h4><b>Student class:</b>". $class_nam. "</h4>";
                    echo '<br>';
                    echo "<table class='table'>";
                    echo "<tsection>";
                    echo "<tr>";
                    echo "<th>Subject Name</th>";
                    echo "<th>Full Marks</th>";
                    echo "<th>Pass Marks</th>";
                    echo "<th>Obtained Marks</th>";
                    echo "</tr>";

                    echo "</tsection>";
                    echo "<tbody>";

                    foreach($new as $key => $value) {
                        $value['obtained_marks'] = round(($value['obtained_marks']) / ($value['final_fullmark'] / $value['subject_fullmark']),2);
                        echo "<tr>";
                        echo "<td>" . $key . "</td>";
                        echo "<td>" . $value['subject_fullmark'] . "</td>";
                        echo "<td>" . $value['subject_passmark'] . "</td>";
                        echo "<td>" . $value['obtained_marks'] . "</td>";
                        echo "</tr>";
                        $total_marks = $total_marks+$value['obtained_marks'];
                        $total_fullmark = $total_fullmark +$value['subject_fullmark'];
                    }
                        $percentage = (($total_marks/$total_fullmark)*100);



                    echo "</tbody>";
                    echo "</table>";
                    echo "<b>Total Obtained marks is:</b> $total_marks";
                    echo "<br>";
                    echo "<br>";
                    echo "<b>Percentage secured is : </b>",number_format((float)$percentage, 2, '.', '.'),'%';


                    echo "<br>";

                    if ($percentage > 80) {
                        echo "<p align='center'>Congratulations!! have secured distinction</p>";
                    } elseif ($percentage > 60 && $percentage< 80) {
                        echo "<p align='center'>Congratulations!! its a First division</p>";
                    } else {
                        echo "<p align='center'>Oops! Sorry, you failed this term</p>";
                    }
                    mysqli_free_result($result);
                } else {
                    echo "0 results";
                }
                $conn->close();
                ?>
            </div>
        </div>
    </div>
<?php
include $base . "footer.php";
?>