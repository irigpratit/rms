<?php
$base = '../../includes/';
include $base . "header.php";

if(isset($_GET['exam_id'])) $exam_id=(int)$_GET['exam_id'];
else $exam_id=0;

if(isset($_GET['id'])) $student_id=(int)$_GET['id'];
else $student_id=0;
?>

    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">
                <?php include $base . "common/dbconfig.php";

                $sql = "SELECT
                    m.obtained_marks, c.class_name,sub.subject_name,sub.subject_fullmark, sub.subject_passmark,
                    e.exam_type, s.student_name, s.student_rollnumber
                    FROM marks m
                    JOIN exam e ON e.id=m.exam_id AND e.id=$exam_id
                    JOIN student s ON m.student_id=s.id AND s.id=$student_id
                    JOIN subject sub ON m.subject_id=sub.id
                    JOIN class c ON c.id=m.class_id";
                if($exam_id==7){
                    $sql = "SELECT
                    avg(m.obtained_marks) AS 'obtained_marks', c.class_name,sub.subject_name,sub.subject_fullmark, sub.subject_passmark,
                    e.exam_type, s.student_name, s.student_rollnumber
                    FROM marks m
                    JOIN student s ON m.student_id=s.id AND s.id=$student_id
                    JOIN exam e ON e.id=m.exam_id
                    JOIN subject sub ON m.subject_id=sub.id
                    JOIN class c ON c.id=m.class_id GROUP BY sub.id";
                }

                $result = $conn->query($sql);


                if ($result->num_rows > 0) {

                    echo "<table class='table'>";
                    echo "<tsection>";
                    echo "<tr>";
                    echo "<th>Subject Name</th>";
                    echo "<th>Full Marks</th>";
                    // echo "<th>Avg Marks</th>";
                    echo "<th>Pass Marks</th>";
                    echo "<th>Obtained Marks</th>";
                    echo "</tr>";
                    echo "</tsection>";
                    echo "<tbody>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<tr>";
                        echo "<td>" . $row['subject_name'] . "</td>";
                        echo "<td>" . $row['subject_fullmark'] . "</td>";
                        // echo "<td>" . $row['average'] . "</td>";
                        echo "<td>" . $row['subject_passmark'] . "</td>";
                        echo "<td>" . round($row['obtained_marks'],2) . "</td>";
                        echo "</tr>";
                    }
                    echo "</tbody>";
                    echo "</table>";

                    mysqli_free_result($result);
                } else {
                    echo "0 results";
                }
                $conn->close();
                ?>
            </div>
        </div>
    </div>
<?php
include $base . "footer.php";
?>