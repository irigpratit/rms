<?php
$base = '../../includes/'; ?>

<?php include $base . 'header.php'; ?>
    <div class="main-content container">
        <div class="row">

            <?php
                include $base."common/dbconfig.php";
                $sql="select id,exam_type from exam";
                $result= $conn->query($sql);
                while($row=$result->fetch_assoc())
                {            ?>
            <a href="<?php echo $BASE_URL; ?>class/view_class.php?exam_id=<?php echo $row['id'];?>">
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h4><?php echo $row['exam_type'];?></h4>
                        </div>
                    </div>
                </div>
            </a>
                    <?php
                }
                    ?>
        </div>
    </div>
<?php include $base . 'footer.php'; ?>