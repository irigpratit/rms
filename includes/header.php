<?php

$URL = 'http://localhost/result_management/';
$BASE_URL = 'http://localhost/result_management/application/';
?>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Result Management System</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo $URL; ?>index.php">RMS</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $BASE_URL; ?>student/view_student.php">List</a></li>
                        <li><a href="<?php echo $BASE_URL; ?>student/add_student.php">Add</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Class</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $BASE_URL; ?>class/view_class.php">List</a></li>
                        <li><a href="<?php echo $BASE_URL; ?>class/add_class.php">Add</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Subject</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $BASE_URL; ?>subject/view_subject.php">List</a></li>
                        <li><a href="<?php echo $BASE_URL; ?>subject/add_subject.php">Add</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Exam</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $BASE_URL; ?>exam/view_exam.php">List</a></li>
                        <li><a href="<?php echo $BASE_URL; ?>exam/add_exam.php">Add</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Marks</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $BASE_URL; ?>marks/view_marks.php">List</a></li>
                        <li><a href="<?php echo $BASE_URL; ?>marks/add_marks.php">Add</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</div>
