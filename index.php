<?php include "includes/header.php"; ?>
    <div class="main-content container">
        <div class="row">
            <a href="application/student/view_student.php">
                <div class="col-lg-3">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h4>View Students</h4>
                        </div>
                    </div>
                </div>
            </a>

            <a href="application/class/view_class.php">
                <div class="col-lg-3">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                            <h4>View Classes</h4>
                        </div>
                    </div>
                </div>
            </a>
            <a href="application/subject/view_subject.php">
                <div class="col-lg-3">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <h4>View Subjects</h4>
                        </div>
                    </div>
                </div>
            </a>

            <a href="application/result/view_result.php">
                <div class="col-lg-3">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h4>View Result</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
<?php include "includes/footer.php"; ?>